Product page URL Link - https://juniorwebtest.000webhostapp.com/index.php

<!DOCTYPE html>
<html>
<head>
		<link href="style.css" rel="stylesheet" type="text/css">
	<title>"Product Page"</title>
	 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
  	.add{
	color: black;
	position: fixed;
	top: 20px;
	right: 150px;
	padding: 10px 16px;
	background:white;
}
  </style>
</head>
<body style="background: #efefef;">
	<div class="container py-5">
<h1 style="color: black; text-align: left;">
	Product List
</h1>
<a href="add.php">
					<input type="submit" name="ADD" value="ADD" class="add">
				</a><br><br>
<form action="index.php" method="POST">	
	<input type="submit" name="prdct-dlt-btn-warning" id="delete-product-btn" value="MASS DELETE" style="color: black; background:red; position: fixed; top: 20px; right: 10px; padding: 10px 16px;">
	<div class="row-mt-4" >
		<?php
		  require ('connect.php');

		  $query = "SELECT * FROM products";
		  $query_run = mysqli_query($conn,$query);
		  $check_product = mysqli_num_rows($query_run) > 0;

		  if ($check_product) 
		  {
		  	while ($row = mysqli_fetch_assoc($query_run))
		  	 {
		  	 	?>
		  	 	<div class="col-md-3 mt-4">
			<div class="card" style="width: 200px ; border-style: ridge; padding: 20px; background: white;">
				<div class="card-body">
					<input type="checkbox" name="product_dlt[]" class="delete-checkbox" value="<?php echo $row["pid"]; ?>"> 
						<p class="card-text" style="text-align: center;">

							<div class="pid" style="display: none;">
							<?php echo $row["pid"]; ?>
							</div>

							<div style="text-align:center;">
							<?php echo $row['sku']; ?>
							</div>

							<div style="text-align:center;">
							<?php echo $row['name']; ?>
							</div>

							<div style="text-align:center;">
							<?php echo  $row['price'] . ".00 $"; ?>
						    </div>

							<div style="text-align: center;">
							<?php if (isset($row['size']) && !empty($row['size'])) {
								echo "Size:" . $row['size'] . "MB";
							} else {
							  echo "" ;
							}?>
							</div>

							<div style="text-align: center;">
							<?php if (isset($row['width']) && !empty($row['width'])) {
							  echo "Dimension:" . $row['width']. "x"; 
							      echo $row['height'] . "x"; 
							      echo $row['length'];
							      } else {
							      	echo "";
							      } ?>
							</div>

							<div style="text-align: center;">
							<?php if (isset($row['weight']) && !empty($row['weight'])) {
								echo "Weight:" .  $row['weight'] . "KG"; 
							} else {
							 echo "" ;
							} ?>
							</div>	

						</p>
				</div>
			</div>
		</div>
		  	 	<?php
		  	 }
		  }
		  else {
		  	echo "No products found";
		  }
		?> 
	</div>
</div>
</form>	
<footer style="text-align:center;">Scandiweb Test Assignment</footer>
</body>
</html>