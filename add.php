Add product Page URL Link - https://juniorwebtest.000webhostapp.com/add.php

<!DOCTYPE html>
<html>
<?php

include ('connect.php');

?>
<head>
	<title>Add Product</title>
    <style >
      .product{
        display: none;
      }
      #cancel{
        text-align: center;
        background: yellow;
        color: black;
      }
    </style>
</head>
<body style="background: #efefef;">
    <h1>  Add Product  </h1>
    <a href="index.php"> <input type="submit" name="submit" value="Cancel" style="position: fixed; top: 20px; right: 10px; padding: 10px 16px; background: white; ">  </a>  
    <form action="index.php" method="POST" id="product_form" autocomplete="OFF">
       <input type="submit" name="submit" value="Save" style="position: fixed; top: 20px; right: 100px; padding: 10px 16px; background: white; ">
<label  >SKU: <input type="text" name="sku" id="sku" required placeholder="Type SKU"> </label><br/><br/>
<label > Name: <input type="text"  name="name" id="name" required placeholder="Type Name"></label><br/><br/>
<label >Price($): <input type="number" name="price" id="price" required placeholder="Type Price"></label><br/><br/>
<label>Type Switcher: </label>

<select name="typeswitcher" id="productType" class="productType">
    <option value="typeswitcher" id="typeswitcher">-Type Switcher-</option>
    <option value="dvd" >DVD</option>
    <option value="furniture" >Furniture</option>
    <option value="book" >Book</option>
  </select>

<div id="dvd" class="product">
  <p>
    <input type="text" name="size" id="size" placeholder="Size(MB)" >
    <br><br>Please Provide disc size(mb)
</div>
  </p>
  <p>
    <div id="furniture" class="product" >
    <input type="text" name="width" id="width" placeholder="Width(CM) ">
    <input type="text" name="height" id="height" placeholder="Height(CM)">
    <input type="text" name="length" id="length" placeholder="Length(CM)">
     <br><br>Please Provide dimensions in WxHxL format
  </div>
  </p>
    <p>
    <div id="book" class="product">
    <input type="text" name="weight" id="weight" placeholder="Weight(KG)">
    <br><br>Please provide book weight(KG)
  </div>
  </p>
 
</form><hr>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
  $(document).ready(function(){
    $("#productType").on('change',function(){
      $(".product").hide();
     $("#" + $(this).val()).fadeIn();
    }).change();
  });
</script>
</body>
<footer style="text-align:center;">Scandiweb Test Assignment</footer> 
</html>